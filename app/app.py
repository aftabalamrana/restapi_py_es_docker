from os import environ
from flask import Flask, jsonify, make_response
from .handlers.routes import configure_routes
import settings

# -- Application initialization. ---------------------------------------------
__modeConfig__ = environ.get('MODE_CONFIG') or 'Development'
app = Flask(__name__)
app.config.from_object(getattr(settings, __modeConfig__.title()))
configure_routes(app)


# -- This function controls how to respond to common errors. -----------------
@app.errorhandler(404)
def not_found(error):
    """ HTTP Error 404 Not Found """
    headers = {}
    return make_response(
        jsonify(
            {
                'error': 'true',
                'msg': str(error)
            }
        ), 404, headers
    )


@app.errorhandler(405)
def not_allowed(error):
    """ HTTP Error 405 Not Allowed """
    headers = {}
    return make_response(
        jsonify(
            {
                'error': 'true',
                'msg': str(error)
            }
        ), 405, headers
    )


@app.errorhandler(500)
def internal_error(error):
    """ HTTP Error 500 Internal Server Error """
    headers = {}
    return make_response(
        jsonify(
            {
                'error': 'true',
                'msg': str(error)
            }
        ), 500, headers
    )


# -- This piece of code controls what happens during the HTTP transaction. ---
@app.before_request
def before_request():
    """ This function handles  HTTP request as it arrives to the API """
    pass


@app.after_request
def after_request(response):
    """ This function handles HTTP response before send it back to client  """
    return response


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=8009)
