import random
from os import environ


class Ticket(object):
    def __init__(self, no_of_lines: int = 0, closed=None, lines=None):
        if lines is None:
            self.closed = False
            self.lines = []
            self.add_lines(no_of_lines)
        else:
            self.closed = closed
            self.lines = lines

    def add_lines(self, no_of_lines=environ.get('NO_OF_LINES')):
        def line_element(): return random.randint(int(environ.get('LINE_MIN_VAL')), int(environ.get('LINE_MAX_VAL')))

        def line():
            result = []
            for i in range(0, int(environ.get('ELEMENTS_IN_LINE'))):
                result.append(line_element())
            return result

        for i in range(0, no_of_lines):
            self.lines.append(line())

    def sort_lines_by_result(self):
        self.lines.sort(reverse=True, key=self.line_result)

    def line_result(self, line):
        # we can check 'line' type, size etc or any other checks
        if len(line) != int(environ.get('ELEMENTS_IN_LINE')):
            return 0

        result = 0
        if line[0] + line[1] + line[2] == 2:
            result = 10
        elif line[0] == line[1] and line[1] == line[2]:
            result = 5
        elif line[0] != line[1] and line[0] != line[2]:
            result = 1
        return result

