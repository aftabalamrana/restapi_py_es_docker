from elasticsearch import Elasticsearch, exceptions


class PoppuloDB(object):
    index = None

    def __init__(self, es_server, index):
        self.es_server = es_server
        self.index = index
        self.create_index()

    # datastore default functions
    def create_index(self):
        # create index for to store 'codeinstitute' data
        return self.execute(method='create_index', ignore=400)

    def create_doc(self, *args, **kargs):
        # insert/update data in our codeinstitute index
        return self.execute(method='insert_docs', *args, **kargs)

    def retrieve_doc(self, *args, **kargs):
        # insert/update data in our codeinstitute index
        return self.execute(method='retrieve_doc', *args, **kargs)

    def retrieve_docs(self, *args, **kargs):
        # insert/update data in our codeinstitute index
        return self.execute(method='retrieve_docs', *args, **kargs)

    def update_docs(self, *args, **kargs):
        # insert/update data in our codeinstitute index
        return self.execute(method='update_docs', *args, **kargs)

    def execute(self, method="", *args, **kargs):
        try:
            # get connection with datastore
            es = Elasticsearch(self.es_server)
            # a list of possible methods as per our need
            method_map = {
                'create_index': es.indices.create,
                'exists_index': es.indices.exists,
                'insert_docs': es.index,
                'update_docs': es.update,
                'retrieve_docs': es.search,
                'retrieve_doc': es.get
            }

            result = method_map[method](index=self.index, *args, **kargs)
            return result
        except exceptions.NotFoundError:
            # raise Elasticsearch.exceptions.NotFoundError(f"Error: {e.info}")
            pass
