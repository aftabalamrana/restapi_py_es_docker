import json
from os import environ
from flask import Response, request
# from app.app import Ticket
# from app.app import PoppuloDB
from .models.ticket import Ticket
from .services.es_datastore import PoppuloDB


def configure_routes(app):
    es_db = PoppuloDB(es_server=environ.get('ES_SERVER'), index=environ.get('INDEX'))

    @app.route('/')
    def welcome():
        # this is just to check status of API
        return 'Welcome to Poppulo lottery Api'

    @app.route('/ticket/', methods=['POST'])  # Create a ticket
    def create_ticket():
        try:
            no_of_lines = request.json.get('no_of_lines', environ.get('NO_OF_LINES'))
            if isinstance(no_of_lines, int):
                t = Ticket(no_of_lines)
                res = es_db.create_doc(body=json.loads(json.dumps(t.__dict__)))
                t.id = res['_id']
                return Response(json.dumps({'ticket': t.__dict__}), status=201, mimetype='application/json')
            else:
                return Response(json.dumps({'message': environ.get('ERR_INVALID_INPUT')}), status=400, mimetype='application/json')
        except Exception as err:
            raise err

    @app.route('/ticket/', methods=['GET'])  # Return list of tickets
    def get_tickets():
        try:
            body = {"query": {"match_all": {}}}  # or simply pass {}, select all... we are not passing and filter
            result = es_db.retrieve_docs(body=body, size=10000)  # create cursor if size is > 10k
            res = []
            if result['hits'] and result['hits']['hits']:
                for hit in result['hits']['hits']:
                    t = hit["_source"]
                    t['id'] = hit['_id']
                    res.append(t)
                return Response({json.dumps(res)}, status=200, mimetype='application/json')
            else:
                return Response(json.dumps({'message': environ.get('DATA_NOT_FOUND')}), status=200, mimetype='application/json')
        except Exception as err:
            raise err

    @app.route('/ticket/<ticket_id>', methods=['GET'])  # Get individual ticket
    def get_ticket(ticket_id):
        try:
            result = es_db.retrieve_doc(id=ticket_id)
            if result and result['found']:
                t = result['_source']
                t['id'] = result['_id']
                return Response({json.dumps(t)}, status=200, mimetype='application/json')
            else:
                return Response(json.dumps({'message': environ.get('ERR_INVALID_INPUT')}), status=400, mimetype='application/json')
        except Exception as err:
            raise err

    @app.route('/ticket/<ticket_id>', methods=['PUT'])  # Amend ticket lines
    def amend_ticket(ticket_id):
        try:
            no_of_lines = request.json.get('no_of_lines', environ.get('NO_OF_LINES'))
            if isinstance(no_of_lines, int):
                result = es_db.retrieve_doc(id=ticket_id)
                if result and result['found']:
                    if not result['_source']['closed']:
                        ticket = Ticket(closed=result['_source']['closed'], lines=result['_source']['lines'])
                        ticket.add_lines(no_of_lines)
                        res = es_db.update_docs(id=ticket_id, body={"doc": ticket.__dict__})
                        # log 'res' if needed
                        t = ticket.__dict__
                        t['id'] = ticket_id
                        return Response({json.dumps(t)}, status=200, mimetype='application/json')
                    else:
                        return Response(json.dumps({'message': environ.get('ERR_TICKET_CLOSED')}), status=400, mimetype='application/json')
                else:
                    return Response(json.dumps({'message': environ.get('ERR_INVALID_INPUT')}), status=400, mimetype='application/json')
            else:
                return Response(json.dumps({'message': environ.get('ERR_INVALID_INPUT')}), status=400, mimetype='application/json')
        except Exception as err:
            raise err

    @app.route('/status/<ticket_id>', methods=['PUT'])  # Retrieve status of ticket
    def status_ticket(ticket_id):
        try:
            result = es_db.retrieve_doc(id=ticket_id)
            if result and result['found']:
                ticket = Ticket(closed=True, lines=result['_source']['lines'])
                ticket.sort_lines_by_result()
                t = ticket.__dict__
                t['id'] = ticket_id
                res = es_db.update_docs(id=ticket_id, body={"doc": t})
                return Response({json.dumps(t)}, status=200, mimetype='application/json')
            else:
                return Response(json.dumps({'message': environ.get('ERR_INVALID_INPUT')}), status=400, mimetype='application/json')
        except Exception as err:
            raise err
