from os import path
from environs import Env

ENV_FILE = path.join(path.dirname(path.dirname(path.abspath(__file__))), '.env')

if path.exists(ENV_FILE):
    ENVIR = Env()
    ENVIR.read_env()
else:
    print('Error: .env file not found')
    exit(code=1)

print(ENVIR('MODE_CONFIG'))


from os import environ
__modeConfig__ = environ.get('MODE_CONFIG') or 'Development'

print(__modeConfig__)

print(environ.get('DATA_NOT_FOUND'))