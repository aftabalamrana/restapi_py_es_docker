import random

line_val1 = lambda: random.randint(0, 2)

print(line_val1())
print(line_val1())
print(line_val1())


def line_number(): return random.randint(0, 2)


def line(ELEMENTS_IN_LINE):
    result = []
    for i in range(0, ELEMENTS_IN_LINE):
        result.append(line_number())
    return result

ELEMENTS_IN_LINE = 3
print(line(ELEMENTS_IN_LINE))