def line_result(line):
    # we can check 'line' type, size etc or any other checks
    if len(line) != int('3'):
        return 0

    result = 0
    if line[0] + line[1] + line[2] == 2:
        result = 10
    elif line[0] == line[1] and line[1] == line[2]:
        result = 5
    elif line[0] != line[1] and line[0] != line[2]:
        result = 1
    return result


def sort_by_line_result(e): return line_result(e)


lines = [
    [0, 0, 0],
    [0, 0, 1],
    [0, 0, 2],
    [0, 1, 0],
    [0, 1, 1],
    [0, 1, 2],
    [0, 2, 0],
    [0, 2, 1],
    [0, 2, 2],
    [1, 0, 0],
    [1, 0, 1],
    [1, 0, 2],
    [1, 1, 0],
    [1, 1, 1],
    [1, 1, 2],
    [1, 2, 0],
    [1, 2, 1],
    [1, 2, 2],
    [2, 0, 0],
    [2, 0, 1],
    [2, 0, 2],
    [2, 1, 0],
    [2, 1, 1],
    [2, 1, 2],
    [2, 2, 0],
    [2, 2, 1],
    [2, 2, 2]
]

lines.sort(reverse=True, key=sort_by_line_result)
print(lines)
