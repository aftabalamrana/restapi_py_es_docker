"""
This is a test file for pytest
"""
import pytest
import requests
from flask import Flask, request


TESTAPP = Flask(__name__)

### /status/<ticket_id> PUT Retrieve status of ticket
def test_put_status_of_ticket_status_code_equals_200():
    url = 'http://0.0.0.0:8009/status/unagMXcBx7DHZlygHzU0'
    payload = {}
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("PUT", url, headers=headers, data=payload)

    assert response.status_code == 200
