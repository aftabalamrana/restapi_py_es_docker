"""
This is a test file for pytest
"""
import pytest
import requests
from flask import Flask, request


TESTAPP = Flask(__name__)

# /ticket GET Return list of tickets
def test_get_tickets_status_code_equals_200():
    url = 'http://0.0.0.0:8009/ticket/'
    payload = {}
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    assert response.status_code == 200
