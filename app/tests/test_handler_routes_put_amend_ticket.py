"""
This is a test file for pytest
"""
import pytest
import requests
from flask import Flask, request


TESTAPP = Flask(__name__)

# /ticket/<ticket_id> PUT Amend ticket lines
def test_put_amend_ticket_status_code_equals_200():
    url = 'http://0.0.0.0:8009/ticket/unagMXcBx7DHZlygHzU0'
    payload = '{"no_of_lines": 1}'
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("PUT", url, headers=headers, data=payload)

    assert response.status_code == 200
