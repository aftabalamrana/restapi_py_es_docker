"""
This is a test file for pytest
"""
import pytest
import requests
from flask import Flask, request


TESTAPP = Flask(__name__)

# /ticket POST Create a ticket:
def test_post_create_a_ticket_status_code_equals_201():
    url = 'http://0.0.0.0:8009/ticket/'
    payload = '{"no_of_lines": 1}'
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 201
