"""
This is a test file for pytest
"""
import pytest
import requests
from flask import Flask, request


TESTAPP = Flask(__name__)


@pytest.fixture
def test_client():
    """ This sets testing mode """
    TESTAPP.config['TESTING'] = True


def test_string_index():
    """ This will test the echo endpoint for a specific behavior"""
    with TESTAPP.test_request_context('/'):
        assert request.path == '/'


def test_get_welcome_status_code_equals_200():
    response = requests.get("http://localhost:8009")
    assert response.status_code == 200
