# Architecture and basics of service
[Resful API Requirements can be found here (... in docs directory)](../docs/Tech_Task_-_Software_Engineer.pdf)

I used elasticsearch (es) as datastore considering type of data (I found es interesting for the exercise).

## Tools and Techs used in API
- Python:3.8
- flask:1.1.2
- elasticsearch:7.10.1
- pytest:6.2.1

## Development environment (python, MacOS)
- create venv
- select interpratpr
- install requirements ```pip install -r requirements.txt```

## Run Tests
To Run API test please run following command in terminal from the root directory of source code (make sure application running)
```bash
pytest
```
Unit test result:
```git
=========================================================================== test session starts ============================================================================
platform darwin -- Python 3.8.6, pytest-6.2.1, py-1.10.0, pluggy-0.13.1
rootdir: /Users/aftab/aftab-gitlab/codeinstitute_py_es/app
collected 8 items                                                                                                                                                          

tests/test_echo.py .                                                                                                                                                 [ 12%]
tests/test_handler_routes_get_ticket.py .                                                                                                                            [ 25%]
tests/test_handler_routes_get_tickets.py .                                                                                                                           [ 37%]
tests/test_handler_routes_get_welcome.py ..                                                                                                                          [ 62%]
tests/test_handler_routes_post_create_ticket.py .                                                                                                                    [ 75%]
tests/test_handler_routes_put_amend_ticket.py .                                                                                                                      [ 87%]
tests/test_handler_routes_put_status_of_ticket.py .                                                                                                                  [100%]

============================================================================ 8 passed in 1.40s =============================================================================
```

## Setup Poppulo lottery API
Before API deployment; we need datastore which is elasticsearch in our case.
### 1. Backend (elasticsearh)
We have two options either install directly from following url or deploy in docker container.
1) [install elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html)
2) To Install elasticsearch follow [install docker](https://docs.docker.com/install/) link or run following commands in terminal.
```
docker pull elasticsearch:7.10.1
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.10.1
```
After running above commands you can find elasticsearch server at http://localhost:9200. 
You can use any existing es server this API it will not affect and existing data 
just make sure 'index' name is not already in use at your server, check .env file.

### 2. Build a API container image
Update server information in [.env](app/.env) file in source code folder (```export ES_SERVER = 'http://yourserver:9200/'```) 
Open terminal window (Mac, Linux) or CMD window (Windows) and place yourself
in the directory where the source code of your project is. Then, type the 
following command.
```shell
docker build . -t codeinstitute_api:0.0.0
```
where:
  - "." represents the current directory where you are
  - "-t" means you want to tag your container image with name
  - "codeinstitute_api" is the main name of your container image
  - "0.0.0" is the part of the tag name related to the container version

### 3. How to run a container image
Open a Terminal window (Mac, Linux) or CMD window (Windows) and type:

```shell
docker run -p 8009:8009 --name codeinstitute_api codeinstitute_api:0.0.0
```

where:
  - "codeinstitute_api:0.0.0" is the local image you want to run as a container
  - "codeinstitute_api_container" is the name you want to assign to this container image 
    when it is run.

### API can be tested by following code:

### API URL input/output:
Request: [http://localhost:8009](http://localhost:8009)
Method: GET
```json
'headers': {
  'Content-Type': 'application/json'
}
```
Response
Status: 200 OK
```Welcome to Poppulo lottery Api```


### /ticket POST Create a ticket:
Request: [http://localhost:8009/ticket/](http://localhost:8009/ticket/)
Method: POST
```json
'headers': {
  'Content-Type': 'application/json'
}

'payload': {
  "no_of_lines":1
}
```
Response
Status: 201 CREATED
```json
{
    "ticket": {
        "closed": false,
        "lines": [
            [
                2,
                1,
                1
            ]
        ],
        "id": "o9XyL3cBYgGLGOaXV9-c"
    }
}
```


### /ticket GET Return list of tickets
Request: [http://localhost:8009/ticket/](http://localhost:8009/ticket/)
Method: GET
```json
'headers': {
  'Content-Type': 'application/json'
}
```
Response
Status: 200 OK
```json
[
    {
        "closed": true,
        "lines": [
            [
                0,
                2,
                0
            ],
            [
                1,
                2,
                1
            ]
        ],
        "id": "ndXqL3cBYgGLGOaXGd8r"
    },
    {
        "closed": true,
        "lines": [
            [
                1,
                1,
                0
            ],
            [
                0,
                2,
                0
            ],
            [
                0,
                1,
                0
            ],
            [
                2,
                1,
                2
            ],
            [
                2,
                2,
                1
            ],
            [
                2,
                0,
                2
            ]
        ],
        "id": "ntXsL3cBYgGLGOaXbd9z"
    },
    {
        "closed": true,
        "lines": [
            [
                0,
                0,
                2
            ],
            [
                1,
                1,
                1
            ],
            [
                2,
                2,
                2
            ],
            [
                0,
                2,
                1
            ],
            [
                0,
                1,
                2
            ],
            [
                1,
                2,
                1
            ]
        ],
        "id": "n9XuL3cBYgGLGOaXyt8a"
    }
]
```


### /ticket/<ticket_id> GET individual ticket
Request: [http://localhost:8009/ticket/pNUvMHcBYgGLGOaXt9_Q](http://localhost:8009/ticket/pNUvMHcBYgGLGOaXt9_Q)
Method: GET
```json
'headers': {
  'Content-Type': 'application/json'
}
```
Response
Status: 200 OK
```json
{
    "closed": false,
    "lines": [
        [
            0,
            2,
            2
        ]
    ],
    "id": "pNUvMHcBYgGLGOaXt9_Q"
}
```

### /ticket/<ticket_id> PUT Amend ticket lines
Request: [http://localhost:8009/ticket/pNUvMHcBYgGLGOaXt9_Q](http://localhost:8009/ticket/pNUvMHcBYgGLGOaXt9_Q)
Method: PUT
```json
'headers': {
  'Content-Type': 'application/json'
}

'payload': {
  "no_of_lines": 5
}
```
Response
Status: 200 OK
```json

{
    "closed": false,
    "lines": [
        [
            2,
            1,
            1
        ],
        [
            0,
            0,
            1
        ],
        [
            2,
            1,
            0
        ],
        [
            2,
            2,
            1
        ],
        [
            1,
            2,
            1
        ],
        [
            1,
            1,
            1
        ],
        [
            1,
            2,
            0
        ],
        [
            1,
            1,
            1
        ],
        [
            2,
            1,
            0
        ],
        [
            0,
            0,
            0
        ],
        [
            0,
            0,
            0
        ]
    ],
    "id": "o9XyL3cBYgGLGOaXV9-c"
}
```

### /status/<ticket_id> PUT Retrieve status of ticket
Request: [http://localhost:8009/status/pNUvMHcBYgGLGOaXt9_Q](http://localhost:8009/ticket/pNUvMHcBYgGLGOaXt9_Q)
Method: PUT
```json
'headers': {
  'Content-Type': 'application/json'
}
```
Response
Status: 200 OK
```json
{
    "closed": true,
    "lines": [
        [
            1,
            1,
            1
        ],
        [
            1,
            1,
            1
        ],
        [
            0,
            0,
            0
        ],
        [
            0,
            0,
            0
        ],
        [
            2,
            1,
            1
        ],
        [
            2,
            1,
            0
        ],
        [
            1,
            2,
            0
        ],
        [
            2,
            1,
            0
        ],
        [
            0,
            0,
            1
        ],
        [
            2,
            2,
            1
        ],
        [
            1,
            2,
            1
        ]
    ],
    "id": "o9XyL3cBYgGLGOaXV9-c"
}
```
